# label-operator

This is the implementation from https://kubernetes.io/blog/2021/06/21/writing-a-controller-for-pod-labels/

## How to build

Run:

    go mod tidy
    go get
    make build
    make docker-build

## How to test in Kubernetes

Run:

    kubectx ds2
    kubens infra # your test namespace
    make run

In a second terminal, run:

    kubectl run --image=nginx my-nginx
    kubectl get pod my-nginx --show-labels # should not show the extra label
    kubectl annotate pod my-nginx ds-2.de/add-pod-name-label=true
    kubectl get pod my-nginx --show-labels # now it should show the extra label
